package com.epam.coffeemachine.interfaces;

import com.epam.coffeemachine.model.Coffee.Coffee;

public interface IStorage {
    boolean checkQuantity(Coffee coffee);
    boolean checkPrice(Coffee coffee, double userPayment);
    double getCoffeePrice(Coffee coffee);
}
