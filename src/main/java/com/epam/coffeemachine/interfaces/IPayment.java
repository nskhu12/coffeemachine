package com.epam.coffeemachine.interfaces;

public interface IPayment {
    double makePayment(double price, double amountPayed);
}
