package com.epam.coffeemachine.main;

import com.epam.coffeemachine.interfaces.IPayment;
import com.epam.coffeemachine.interfaces.IStorage;
import com.epam.coffeemachine.model.CashBox;
import com.epam.coffeemachine.model.CoffeeMachine;
import com.epam.coffeemachine.model.SimpleStorage;
import com.epam.coffeemachine.model.UserInterface;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        IStorage storage = new SimpleStorage(100, 100, 100, 2, 3, 4);
        IPayment payment = new CashBox();
        UserInterface ui = new UserInterface();
        CoffeeMachine cm = new CoffeeMachine("CMT612", payment, storage, ui);
        cm.start(scanner);
        cm.end();
        scanner.close();
    }
}