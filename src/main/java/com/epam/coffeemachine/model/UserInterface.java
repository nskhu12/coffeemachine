package com.epam.coffeemachine.model;

import java.util.Scanner;

public class UserInterface {

    public UserInterface() {
    }

    public void greetToUser(){
        System.out.println("Welcome to simple coffee machine.");;
    }

    public void thankToUser(){
        System.out.println("See you later.");
    }

    public String askUserForCoffeeType(Scanner scanner){
        System.out.println("Please enter your preferred coffee type (mocha/espresso/latte/americano/cappuccino):");
        String coffeeType = scanner.nextLine();
        return coffeeType;
    }

    public double askUserForSugarQuantity(Scanner scanner){
        System.out.println("How many spoons of sugar would you like in your coffee?");
        double sugarQuantity = scanner.nextDouble();
        return sugarQuantity;
    }

    public double askUserForPayment(double coffeePrice, Scanner scanner){
        System.out.println("The coffee costs: $" + coffeePrice);
        System.out.println("Please enter the amount of your payment:");
        double payment = scanner.nextDouble();
        return payment;
    }

    public void informUserInsufficientMaterials() {
        System.out.println("Apologies, we are currently unable to serve your requested beverage. We have insufficient coffee, sugar, or milk.");
    }

    public void informUserInsufficientPayment() {
        System.out.println("Apologies, your payment is insufficient. Please provide the correct amount to complete your order.");
    }

    public void informUserChange(double change) {
        System.out.println("Here is your change: $" + change);
    }
}
