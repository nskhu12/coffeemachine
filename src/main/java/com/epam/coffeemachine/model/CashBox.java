package com.epam.coffeemachine.model;

import com.epam.coffeemachine.interfaces.IPayment;

public class CashBox implements IPayment {
    @Override
    public double makePayment(double price, double amountPayed) {
        // make payment
        return amountPayed - price;
    }
}
