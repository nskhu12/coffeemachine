package com.epam.coffeemachine.model;

import com.epam.coffeemachine.interfaces.IPayment;
import com.epam.coffeemachine.interfaces.IStorage;
import com.epam.coffeemachine.model.Coffee.Coffee;

import java.util.Scanner;

public class CoffeeMachine {
    private String id;
    private IPayment paymentSystem;
    private IStorage storageSystem;
    private UserInterface userInterfaceSystem;

    public CoffeeMachine(String id) {
        this.id = id;
    }

    public CoffeeMachine(String id, IPayment paymentSystem, IStorage storageSystem, UserInterface userInterfaceSystem) {
        this.id = id;
        this.paymentSystem = paymentSystem;
        this.storageSystem = storageSystem;
        this.userInterfaceSystem = userInterfaceSystem;
    }

    public void start(Scanner scanner){
        userInterfaceSystem.greetToUser();
        String coffeeType = userInterfaceSystem.askUserForCoffeeType(scanner);
        double sugarQuantity = userInterfaceSystem.askUserForSugarQuantity(scanner);
        Coffee coffee = CoffeeFactory.createCoffee(coffeeType, sugarQuantity);
        if (!storageSystem.checkQuantity(coffee)){
            userInterfaceSystem.informUserInsufficientMaterials();
            return;
        }
        double coffeePrice = storageSystem.getCoffeePrice(coffee);
        double userPayment = userInterfaceSystem.askUserForPayment(coffeePrice,scanner);
        if (!storageSystem.checkPrice(coffee, userPayment)){
            userInterfaceSystem.informUserInsufficientPayment();
            return;
        }
        double change = paymentSystem.makePayment(coffeePrice, userPayment);
        if (change > 0) {
            userInterfaceSystem.informUserChange(change);
        }
        coffee.prepareCoffee();
    }

    public void end(){
        userInterfaceSystem.thankToUser();
    }

    public String getId() {
        return id;
    }

    public IPayment getPaymentSystem() {
        return paymentSystem;
    }

    public void setPaymentSystem(IPayment paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    public IStorage getStorageSystem() {
        return storageSystem;
    }

    public void setStorageSystem(IStorage storageSystem) {
        this.storageSystem = storageSystem;
    }

    public UserInterface getUserInterfaceSystem() {
        return userInterfaceSystem;
    }

    public void setUserInterfaceSystem(UserInterface userInterfaceSystem) {
        this.userInterfaceSystem = userInterfaceSystem;
    }
}
