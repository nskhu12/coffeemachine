package com.epam.coffeemachine.model;

import com.epam.coffeemachine.interfaces.IStorage;
import com.epam.coffeemachine.model.Coffee.Coffee;

public class SimpleStorage implements IStorage {
    private double milkQuantity;
    private double coffeeQuantity;
    private double sugarQuantity;
    private double milkPrice;
    private double coffeePrice;
    private double sugarPrice;

    public SimpleStorage(double milkQuantity, double coffeeQuantity, double sugarQuantity, double milkPrice,
                         double coffeePrice, double sugarPrice) {
        this.milkQuantity = milkQuantity;
        this.coffeeQuantity = coffeeQuantity;
        this.sugarQuantity = sugarQuantity;
        this.milkPrice = milkPrice;
        this.coffeePrice = coffeePrice;
        this.sugarPrice = sugarPrice;
    }

    public SimpleStorage() {
    }

    @Override
    public boolean checkQuantity(Coffee coffee)
    {
        return (coffee.getCoffeeQuantity() <= coffeeQuantity && coffee.getMilkQuantity() <= milkQuantity
                && coffee.getSugarQuantity() <= sugarQuantity);
    }

    @Override
    public boolean checkPrice(Coffee coffee, double userPayment) {
        return getCoffeePrice(coffee) <= userPayment;
    }

    @Override
    public double getCoffeePrice(Coffee coffee) {
        // TODO: 6/26/2023 annotation not null?
        return coffee.getSugarQuantity() * sugarPrice + coffee.getMilkQuantity() * milkPrice
                + coffee.getCoffeeQuantity() * coffeePrice;
    }

    public double getMilkQuantity() {
        return milkQuantity;
    }

    public void setMilkQuantity(double milkQuantity) {
        this.milkQuantity = milkQuantity;
    }

    public double getCoffeeQuantity() {
        return coffeeQuantity;
    }

    public void setCoffeeQuantity(double coffeeQuantity) {
        this.coffeeQuantity = coffeeQuantity;
    }

    public double getSugarQuantity() {
        return sugarQuantity;
    }

    public void setSugarQuantity(double sugarQuantity) {
        this.sugarQuantity = sugarQuantity;
    }

    public double getMilkPrice() {
        return milkPrice;
    }

    public void setMilkPrice(double milkPrice) {
        this.milkPrice = milkPrice;
    }

    public double getCoffeePrice() {
        return coffeePrice;
    }

    public void setCoffeePrice(double coffeePrice) {
        this.coffeePrice = coffeePrice;
    }

    public double getSugarPrice() {
        return sugarPrice;
    }

    public void setSugarPrice(double sugarPrice) {
        this.sugarPrice = sugarPrice;
    }
}
