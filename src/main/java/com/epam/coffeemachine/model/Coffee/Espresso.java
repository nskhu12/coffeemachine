package com.epam.coffeemachine.model.Coffee;

public class Espresso extends Coffee{
    public Espresso(double sugarQnt) {
        super("Espresso");
        super.setCoffeeQuantity(1);
        super.setMilkQuantity(0);
        super.setSugarQuantity(sugarQnt);
    }

    @Override
    public void prepareCoffee() {

    }
}
