package com.epam.coffeemachine.model.Coffee;

public class Cappuccino extends Coffee{
    public Cappuccino(double sugarQnt) {
        super("Cappuccino");
        super.setCoffeeQuantity(2);
        super.setMilkQuantity(2);
        super.setSugarQuantity(sugarQnt);
    }

    @Override
    public void prepareCoffee() {

    }
}
