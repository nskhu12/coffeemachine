package com.epam.coffeemachine.model.Coffee;

public class Americano extends Coffee{
    public Americano(double sugarQnt) {
        super("Americano");
        super.setCoffeeQuantity(2);
        super.setMilkQuantity(1);
        super.setSugarQuantity(sugarQnt);
    }

    @Override
    public void prepareCoffee() {

    }
}
