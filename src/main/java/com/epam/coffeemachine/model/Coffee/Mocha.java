package com.epam.coffeemachine.model.Coffee;

public class Mocha extends Coffee{
    public Mocha(double sugarQnt) {
        super("Mocha");
        super.setCoffeeQuantity(1);
        super.setMilkQuantity(1);
        super.setSugarQuantity(sugarQnt);
    }

    @Override
    public void prepareCoffee() {

    }
}
