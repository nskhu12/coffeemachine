package com.epam.coffeemachine.model.Coffee;

public abstract class Coffee {
    private String name;
    private double milkQuantity;
    private double coffeeQuantity;
    private double sugarQuantity;

    public Coffee(String name) {
        this.name = name;
    }

    public abstract void prepareCoffee();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMilkQuantity() {
        return milkQuantity;
    }

    public void setMilkQuantity(double milkQuantity) {
        this.milkQuantity = milkQuantity;
    }

    public double getCoffeeQuantity() {
        return coffeeQuantity;
    }

    public void setCoffeeQuantity(double coffeeQuantity) {
        this.coffeeQuantity = coffeeQuantity;
    }

    public double getSugarQuantity() {
        return sugarQuantity;
    }

    public void setSugarQuantity(double sugarQuantity) {
        this.sugarQuantity = sugarQuantity;
    }
}
