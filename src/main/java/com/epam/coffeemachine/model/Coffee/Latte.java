package com.epam.coffeemachine.model.Coffee;

public class Latte extends Coffee{
    public Latte(double sugarQnt) {
        super("Latte");
        super.setCoffeeQuantity(2);
        super.setMilkQuantity(3);
        super.setSugarQuantity(sugarQnt);
    }

    @Override
    public void prepareCoffee() {

    }
}
