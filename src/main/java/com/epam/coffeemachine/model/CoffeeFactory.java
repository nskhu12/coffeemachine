package com.epam.coffeemachine.model;

import com.epam.coffeemachine.model.Coffee.*;

public class CoffeeFactory {
    public static Coffee createCoffee(String type, double sugarQuantity) {
        switch (type.toLowerCase()) {
            case "espresso":
                return new Espresso(sugarQuantity);
            case "latte":
                return new Latte(sugarQuantity);
            case "cappuccino":
                return new Cappuccino(sugarQuantity);
            case "mocha":
                return new Mocha(sugarQuantity);
            case "americano":
                return new Americano(sugarQuantity);
            default:
                throw new IllegalArgumentException("Invalid coffee type: " + type);
        }
    }
}




